var express = require('express');
var router = express.Router();
var _ = require('lodash');
var async = require('async');
var Morphy = require('phpmorphy').default;
var morphy = new Morphy('ru', {
//  nojo:                false,
  storage:             Morphy.STORAGE_MEM,
  predict_by_suffix:   true,
  predict_by_db:       true,
  graminfo_as_text:    true,
  use_ancodes_cache:   false,
  resolve_ancodes:     Morphy.RESOLVE_ANCODES_AS_TEXT
});

router.post('/generate', function (req, res) {
  var text = req.body.text;
  var textBaseMap = [];
  var textMap = [];

  async.waterfall([
      prepareText,
      checkMorphy,
      validateText
  ], function (err, map) {
    if (err) { return res.send({ err: err }); }

    return res.json({ map: map })
  });

  function prepareText(cb) {
    var wordsBaseMap = _(text).split(' ');

    cb(null, wordsBaseMap);
  }

  function checkMorphy(text, cb) {
    async.each(text, function (word, next) {
      var wordBaseForm = morphy.lemmatize(word);
      if (!wordBaseForm) { return next(); }

      textBaseMap.push(wordBaseForm);
      textBaseMap = _.uniq(textBaseMap);

      next();
    }, function (err) {
      if (err) { return cb(err); }

      cb()
    });
  }

  function validateText(cb) {
    var wordsCount = _(textBaseMap).countBy().value();
    for (var word in wordsCount) {
      textMap.push({name: word, count: wordsCount[word]});
    }
    textMap = _.filter(textMap, function (word) {
      word.skiped = word.name.length < 3;

      return word;
    });

    textMap = _.remove(textMap, function (word) {
      return !word.skiped;
    });


    var wordsGroup = _.groupBy(textMap, 'count');
    var group = 1;
    var keysGroup = _.keys(wordsGroup).reverse();

    for (var i in keysGroup) {
      var key = keysGroup[i];
      var word = wordsGroup[key];

      for (var j in word) {
        word[j].group = group;
      }

      group++;
    }

    textMap = _.each(textMap, function (word) {
      word.const = word.count * word.group;
      word.constText = (word.count * word.group)/textMap.length;
    });

    cb(null, textMap);
  }
});

module.exports = router;
