(function () {
  'use strict';

  angular
      .module('ciphas')
      .directive('application', application);

  application.$inject = [];

  function application() {
    return {
      replace: true,
      controller: 'ApplicationController',
      controllerAs: 'ApplicationCtrl',
      templateUrl: 'application/application.html'
    };
  }
})();