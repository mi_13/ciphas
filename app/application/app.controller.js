(function () {
  'use strict';

  angular
      .module('ciphas')
      .controller('ApplicationController', ApplicationController);

  ApplicationController.$Inject = [
    '$log', '$mdSidenav',
    'ApplicationUtils'
  ];

  function ApplicationController($log, $mdSidenav,
                                 ApplicationUtils) {
    var vm = this;
    var char = document.getElementById('line');
    char.setAttribute('width', '1000px');
    char.setAttribute('height', '350px');
    var char2 = document.getElementById('line2');
    char2.setAttribute('width', '1000px');
    char2.setAttribute('height', '350px');
    vm.text = 'Нейро́н, или невро́н ( от др.-греч. νεῦρον — волокно, нерв ) — структурно-функциональная единица нервной системы. Нейрон — электрически возбудимая клетка, которая обрабатывает, хранит и передает информацию с помощью электрических и химических сигналов. Нейрон имеет сложное строение и узкую специализацию. Клетка содержит ядро, тело клетки и отростки (дендриты и аксоны). В головном мозге человека насчитывается около 85—86 миллиардов нейронов[1][2]. Нейроны могут соединяться один с другим, формируя биологические нейронные сети. Нейроны разделяют на рецепторные, эффекторные и вставочные.Сложность и многообразие функций нервной системы определяются взаимодействием между нейронами. Это взаимодействие представляет собой набор различных сигналов, передаваемых между нейронами или мышцами и железами. Сигналы испускаются и распространяются с помощью ионов. Ионы генерируют электрический заряд ( потенциал действия ), который движется по телу нейрона.';
    vm.map = [];
    vm.map2 = [];
    vm.activeGraph = false;
    vm.labels = [];
    vm.labels2 = [];
    vm.data = [[]];
    vm.data2 = [[]];
    vm.options = {
      scales: {
        yAxes: [
          {
            id: 'y-axis-1',
            type: 'linear',
            display: true,
            position: 'left'
          }
        ]
      }
    };
    vm.datasetOverride = [{ yAxisID: 'y-axis-1' }];


    vm.exec = function (text) {
      ApplicationUtils.generateTextMap(text)
          .then(function (map) {
            vm.map = map.data.map;

            prepareGraph1();
            prepareGraph2();
            vm.activeGraph = true;
          }, function (err) {
            $log.error(err);
          })
    };

    function prepareGraph1() {
      _.each(vm.map, function (word) {
        vm.labels.push(word.name);
        vm.data[0].push(word.count);
      })
    }

    function prepareGraph2() {
      vm.map2 = _.orderBy(vm.map, 'count');
      _.each(vm.map2, function (word) {
        vm.labels2.unshift(word.name);
        vm.data2[0].unshift(word.count);
      })
    }


  }

})();