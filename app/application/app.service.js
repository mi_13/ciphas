(function () {
  'use strict';

  angular
      .module('ciphas')
      .factory('ApplicationUtils', ApplicationUtils);

  ApplicationUtils.$inject = [
    '$q', '$http'
  ];

  function ApplicationUtils($q, $http) {
    var service = {
      generateTextMap: generateTextMap
    };

    function generateTextMap(text) {
      var defer = $q.defer();

      $http.post('api/textMap/generate', { text: text })
          .then(defer.resolve, defer.reject);

      return defer.promise;

    }

    return service;
  }
})();
