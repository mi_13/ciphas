(function () {
  'use strict';

  angular
      .module('ciphas', [
          'ngMaterial',
          'ngMessages',
          'chart.js'
      ]);
})();
