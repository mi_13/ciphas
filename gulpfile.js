var gulp = require('gulp');
var plumber = require('gulp-plumber');
var del = require('del');
var nodemon = require('gulp-nodemon');


gulp.task('scripts', function () {
  gulp.src('app/**/*.js')
      .pipe(plumber())
      .pipe(gulp.dest('./build/'));
});

gulp.task('express-server', function () {
  nodemon({
    script: 'build/server.js',
    ext: 'js html',
    env: {
      'NODE_ENV': 'development'
    }
  })
});

gulp.task('watch', function () {
  gulp.watch('app/**/*.html', ['build:copyApp']);
  gulp.watch('app/**/*.js', ['scripts']);
  gulp.watch('app/**/*.css', ['build:copyCss']);
});

gulp.task('build:cleanFolder', function (callback) {
  del([
    'build/*'
  ], callback);
});

gulp.task('build:copyApp', function () {
  return gulp.src(['app/**/*.*', '!app/**/*.js'])
      .pipe(gulp.dest('./build/'));
});

gulp.task('build:copyCss', function () {
  return gulp.src('app/**/*.css')
      .pipe(gulp.dest('./build/'));
});

gulp.task('build', ['build:cleanFolder', 'build:copyApp']);
gulp.task('serve', ['scripts', 'build', 'express-server', 'watch']);